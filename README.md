Simple libary to make sending JSON responses easy.

You can set a message, an (int) code and an array of data to be sent, along with an HTTP status code.

All variables can be set on instatiation, or after the fact using getter / setter methods.

`\OllyOllyOlly\Ajax\Response($message = null, $data = null, $code = null, $http = null)`;

### Usage:

```

$success = new \OllyOllyOlly\Ajax\Response\Success("Here is a message!", [
    'foo' => 'bar',
    'bar' => 'foo'
]);

$success->respond();

// Response:

Status code: 200
Content-Type: application/json
{
    "status": "success",
    "code": 1,
    "message": "Here is a message!",
    'data' => [
        "foo": "bar",
        "bar": "foo"
    ],
}


$error = new \OllyOllyOlly\Ajax\Response\Error("Oh no!", [
    'foo' => 'bar',
    'bar' => 'foo'
]);

$error->respond();

// Response:

Status code: 500
Content-Type: application/json
{
    "status": "error",
    "code": 1,
    "message": "Oh no!",
    'data' => [
        "foo": "bar",
        "bar": "foo"
    ],
}


```

### Utility

For utility use `\OllyOllyOlly\Ajax\Response::success` and `\OllyOllyOlly\Ajax\Response::error` to build and respond in one line:

```

\OllyOllyOlly\Ajax\Response::success($data = null, $message = null, $http = null, $code = null);

\OllyOllyOlly\Ajax\Response::error($message = null, $http = null, $data = null, $code = null);

```
