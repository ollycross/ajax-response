#### 2.2.3 (2019-05-13)

##### Chores

* **Typehinting:**  incorrect typehinting for status ([7bfe2127](git+https://gitlab.com/ollycross/ajax-response/commit/7bfe21273c4382352848da94bea326eb3b113f42))

#### 2.2.2 (2019-05-13)

##### Chores

* **Response:**  typo ([65bb9650](git+https://gitlab.com/ollycross/ajax-response/commit/65bb965055afbd2e6fda3d945933fb5fad77c23f))

#### 2.2.1 (2019-05-13)

##### Chores

* **HTTP Codes:**  Make HTTP code contants public ([02b873bf](git+https://gitlab.com/ollycross/ajax-response/commit/02b873bf23e154c48e66b64778311e15537267a6))

### 2.2.0 (2019-05-13)

##### Chores

* **php7:**  Use short array [] syntax ([9baaa49c](git+https://gitlab.com/ollycross/ajax-response/commit/9baaa49cf5033dc2849ff31835b7294b1fa31bd5))
* **HTTP codes:**  Add class constants for HTTP codes ([b348d515](git+https://gitlab.com/ollycross/ajax-response/commit/b348d515f724fc0c2b0225237a000b362eb06651))

#### 2.1.1 (2019-05-13)

##### Chores

* **php7:**  Missing typehinting static in functions ([87d86fab](git+https://gitlab.com/ollycross/ajax-response/commit/87d86fab05032b1abe864f41d3ac5437f4b5a057))

### 2.1.0 (2019-05-13)

##### Chores

* **deploy:**  Add release scripts ([c4ee5766](git+https://gitlab.com/ollycross/ajax-response/commit/c4ee5766b6277f044682046e045f81634ff03aee))

##### Documentation Changes

* **readme:**  Incorrect README info ([1e66ff03](git+https://gitlab.com/ollycross/ajax-response/commit/1e66ff039174c4c55acb473c3198ce22ab699407))

