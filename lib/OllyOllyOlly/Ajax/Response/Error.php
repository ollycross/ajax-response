<?php

namespace OllyOllyOlly\Ajax\Response;

use OllyOllyOlly\Ajax\Response;

class Error extends Response
{
    protected $_status = 'failure';
    protected $_message = 'Unknown Error';
    protected $_code = 0;
    protected $_http = Response::HTTP_INTERNAL_SERVER_ERROR;
}
