<?php

namespace OllyOllyOlly\Ajax\Response;

use OllyOllyOlly\Ajax\Response;

class Success extends Response
{
    protected $_status = 'success';
    protected $_message = 'The request was completed successfully';
    protected $_code = 1;
    protected $_http = Response::HTTP_OK;
}
